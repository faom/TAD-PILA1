/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pila;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author HP
 */
public class pila2 {
    public static void main(String[]args){
        Stack pila2= new Stack();
        string dato;
        boolean uso=true;
        Scanner entrada=new Scanner (System.in);
        while(uso){
     System.out.println("\n\n\t\t\t=========Menú Manejo Pila=============");
     System.out.println("\t\t\t=                                    =");
     System.out.println("\t\t\t= 1- Insertar elemento               =");
     System.out.println("\t\t\t= 2- Eliminar elemento               =");
     System.out.println("\t\t\t= 3- Imprimir pila                   =");
     System.out.println("\t\t\t= 4- Imprimir cima                   =");
     System.out.println("\t\t\t= 5- Buscar elemento                 =");
     System.out.println("\t\t\t= 6- Salir                           =");
     System.out.println("\t\t\t======================================");
     System.out.print("\t\t\tOpcion: ");
     int op = entrada.nextInt();
    switch(op){
                case 1 :
                    dato=entrada.nextInt(); 
                    pila2.push(dato);
                    System.out.println("elemento  "+dato+"  apilado");
                break;
                case 2 :
                    pila2.pop();
                    System.out.println("elemento desapilado");
                break;
                case 3 :
                    if(pila2.empty()){
                        System.out.println("Pila Vacia" );
                    }else{
                        System.out.println("contenido"+ pila2);
                    }
                break;
                case 4: 
                    System.out.println("Cima\n"+pila2.peek());
                break;
                case 5:
                    dato=entrada.nextInt(("Introduzca el dato a buscar"));
                    if(!pila2.contains(dato)){
                        System.out.println("Dato no encontrado");
                    }else{
                         System.out.println(dato +"-- posicion --" + pila2.search(dato));
                    }
                break;
                case 6 :
                    uso=false;
            }
        }
    }
    
}