/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pila;
import java.util.Stack;
import javax.swing.JOptionPane;
/**
 *
 * @author HP
 */
public class Pila {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Stack pila= new Stack();
        int  dato;
        boolean uso=true;
        while(uso){
            String opcion = JOptionPane.showInputDialog("\n1. apilar"+
                                                        "\n2.desapilar"+
                                                        "\n3. imprimir"+
                                                        "\n4. ver cima"+
                                                        "\n5. buscar"+
                                                        "\n6. salir");
            switch(opcion){
                case "1" :
                    dato= Integer.parseInt(JOptionPane.showInputDialog("Introduzca  dato"));
                    pila.push(dato);
                    JOptionPane.showMessageDialog(null,"elemento  "+dato+"  apilado");
                break;
                case "2" :
                    pila.pop();
                    JOptionPane.showMessageDialog(null,"elemento desapilado");
                break;
                case "3" :
                    if(pila.empty()){
                        JOptionPane.showMessageDialog(null,"Pila Vacia"+ JOptionPane.WARNING_MESSAGE);
                    }else{
                        JOptionPane.showMessageDialog(null,"contenido"+ pila);
                    }
                break;
                case "4": 
                    JOptionPane.showMessageDialog(null,"Cima\n"+pila.peek());
                break;
                case "5":
                    dato=Integer.parseInt(JOptionPane.showInputDialog("Introduzca dato a buscar"));
                    if(!pila.contains(dato)){
                        JOptionPane.showMessageDialog(null, "Dato no encontrado"+ JOptionPane.WARNING_MESSAGE);
                    }else{
                         JOptionPane.showMessageDialog(null, dato +"-- posicion --" + pila.search(dato));
                    }
                break;
                case"6" :
                    uso=false;
            }
        }
    }
    
}
